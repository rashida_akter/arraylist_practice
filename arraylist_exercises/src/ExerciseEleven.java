import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseEleven {
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        list.add("abc");
        list.add("bca");
        list.add("cab");
        list.add("aaa");
        list.add("bbb");
        System.out.println(list);
        Collections.reverse(list);
        System.out.println("After reversing: " + list);

    }
}
