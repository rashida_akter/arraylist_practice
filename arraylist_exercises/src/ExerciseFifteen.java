import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseFifteen {
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        list.add("aa");
        list.add("bb");
        list.add("cc");
        List<String> list2 = new ArrayList<String>();
        list2.add("aaa");
        list2.add("bbb");
        list2.add("ccc");
        List<String> addList = new ArrayList<String>();
        addList.addAll(list);
        addList.addAll(list2);
        System.out.println(addList);
    }
}
