import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseTen {
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        list.add("niha");
        list.add("jumana");
        list.add("humaira");
        list.add("samiul");
        list.add("sagor");
        System.out.println(list);
        Collections.shuffle(list);
        System.out.println("After Shuffle: " + list);
    }
}
