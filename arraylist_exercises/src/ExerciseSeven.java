import java.util.ArrayList;
import java.util.List;

public class ExerciseSeven {
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        list.add("aa");
        list.add("bb");
        list.add("cc");
        list.add("dd");
        list.add("ee");
        System.out.println(list);

        if(list.contains("dd")){
            System.out.println("Element Found");
        }else{
            System.out.println("Element unavailable");
        }

    }
}
