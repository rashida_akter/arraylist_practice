import java.util.ArrayList;

public class ExerciseTwentyTwo {
    public static void main(String[] args){
        ArrayList<String> list = new ArrayList<String>();
        list.add("array");
        list.add("loop");
        list.add("switch");
        list.add("do");
        System.out.println(list);
        int numOfElements = list.size();
        for(int i=0; i<numOfElements; i++){
            System.out.println(list.get(i));
        }
    }

}
