import java.util.ArrayList;
import java.util.List;

public class ExerciseThirteen {
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        list.add("aa");
        list.add("bb");
        list.add("cc");
        System.out.println(list);

        List<String> list2 = new ArrayList<String>();
        list2.add("aa");
        list2.add("bb");
        list2.add("cc");
        System.out.println(list2);

        if(list.equals(list2)){
            System.out.println("Both lists are equal");
        }else{
            System.out.println("Both are not equal");
        }
    }
}
