import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseFourteen {
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        list.add("aa");
        list.add("bb");
        list.add("tual");
        list.add("asli");
        System.out.println(list);

        Collections.swap(list, 0, 1);
        System.out.println(list);
    }
}
