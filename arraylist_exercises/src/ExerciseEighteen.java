import java.util.ArrayList;

public class ExerciseEighteen {
    public static void main(String[] args){
        ArrayList<String> list = new ArrayList<String>();
        list.add("Red");
        list.add("Green");
        list.add("Black");
        list.add("White");
        list.add("Pink");
        System.out.println("Original array list: " + list);
        System.out.println(list.isEmpty());
        list.removeAll(list);
        System.out.println(list.isEmpty());
    }
}
