import java.util.ArrayList;
import java.util.Collections;

public class ExerciseTwentyOne {
    public static void main(String[] args){
        ArrayList<String> list = new ArrayList<String>();
        list.add("aa");
        list.add("bb");
        list.add("cc");
        list.add("dd");
        System.out.println("Before using replace method: " + list);
        String newElement = "ee";
        list.set(0, newElement);
        System.out.println(list);

        int num = list.size();
        System.out.println("lists size is:"+num);

        for(int i=0; i<num; i++){
            System.out.println(list.get(i));
        }
    }
}
