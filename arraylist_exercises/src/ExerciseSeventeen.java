import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseSeventeen {
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        list.add("Niha");
        list.add("Ohona");
        list.add("Afif");
        System.out.println(list);
        list.removeAll(list);
        System.out.println("After remove elements: " + list);
    }
}
