import java.util.ArrayList;
import java.util.List;

public class ExerciseFour {
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        list.add("white");
        list.add("blue");
        list.add("black");
        list.add("green");
        System.out.println(list);

        String retriveElement = list.get(0);
        System.out.println(retriveElement);
        retriveElement = list.get(2);
        System.out.println(retriveElement);
    }
}
