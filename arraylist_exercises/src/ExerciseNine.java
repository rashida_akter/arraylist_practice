import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExerciseNine {
    public static void main(String[] args){
        List<String> list1 = new ArrayList<String>();
        list1.add("aa");
        list1.add("bb");
        list1.add("cc");
        list1.add("ee");
        list1.add("dd");
        System.out.println(list1);

        List<String> list2 = new ArrayList<String>();
        list2 = list1;
        Collections.sort(list2);
        System.out.println(list2);
    }
}
