import java.util.ArrayList;
import java.util.List;

public class ExerciseTwo {
    public static void main(String[] args){
        List<String> l = new ArrayList<String>();
        l.add("white");
        l.add("black");
        l.add("yellow");
        l.add("sky blue");

        for(String itr : l){
            System.out.println(itr);
        }
    }
}
