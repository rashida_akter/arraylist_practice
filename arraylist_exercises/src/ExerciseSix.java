import java.util.ArrayList;
import java.util.List;

public class ExerciseSix {
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        list.add("red");
        list.add("green");
        list.add("blue");
        System.out.println(list);

        list.remove(1);
        System.out.println(list);
    }
}
