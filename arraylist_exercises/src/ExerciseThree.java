import java.util.ArrayList;
import java.util.List;

public class ExerciseThree {
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        list.add("black");
        list.add("blue");
        list.add("purple");
        System.out.println(list);

        list.add(0, "pink");
        list.add(3, "yellow");
        System.out.println(list);
    }
}
