import java.util.ArrayList;
import java.util.List;

public class ExerciseFive {
    public static void main(String[] args){
        List<String> list = new ArrayList<String>();
        list.add("red");
        list.add("green");
        list.add("purple");
        System.out.println(list);
        list.set(1, "skyblue");
        System.out.println(list);
    }
}
