import java.util.ArrayList;

public class ExerciseNineteen {
    public static void main(String[] args){
        ArrayList<String> list = new ArrayList<String>();
        list.add("nila");
        list.add("mila");
        list.add("real");
        System.out.println("Original array list: " + list);
        list.trimToSize();
        System.out.println("After applying trim method, the list looks like: " + list);
    }
}
