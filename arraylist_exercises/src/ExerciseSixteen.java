import java.util.ArrayList;

public class ExerciseSixteen {
    public static void main(String[] args){
        ArrayList<String> list = new ArrayList<String>();
        list.add("Red");
        list.add("Green");
        list.add("Black");
        list.add("White");
        list.add("Pink");
        System.out.println("Original array list: " + list);
        ArrayList<String> updatedList = (ArrayList<String>)list.clone();
        System.out.println("Cloned array list: " + updatedList);
    }
}
