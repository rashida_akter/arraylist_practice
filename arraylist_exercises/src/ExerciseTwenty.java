import java.util.ArrayList;

public class ExerciseTwenty {
    public static void main(String[] args){
        ArrayList<String> list = new ArrayList<String>();
        list.add("red");
        list.add("green");
        list.add("blue");
        System.out.println("Before increasing the size, list looks like: " + list);
        list.ensureCapacity(6);
        list.add("light green");
        list.add("light blue");
        list.add("gray");
        System.out.println("After increasing the size, list looks like: " + list);
    }
}
